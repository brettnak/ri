module Remind
  module NutsAndBolts
    class Nut
      attr_reader :size

      def initialize(size)
        @size = size
      end
    end

    class Bolt
      attr_reader :size

      def initialize(size)
        @size = size
      end
    end

    class << self

      # Returns a list of randomly sorted nuts and a list of randomly sorted
      # bolts.  All nuts & bolts have exactly one matching pair in the
      # opposing list.
      def random(n)
        return [
          (0...n).map { |i| Nut.new(i) }.shuffle,
          (0...n).map { |i| Bolt.new(i) }.shuffle,
        ]
      end

      def compare_nut_and_bolt(nut, bolt)
        # No typing in ruby, so to be fair, make sure the
        # nut is a nut and the bolt is a bolt
        if (!nut.is_a?(Nut)) || (!bolt.is_a?(Bolt))
          throw :wrong_types
        end

        return case
        when (nut.size  < bolt.size) then -1
        when (nut.size == bolt.size) then 0
        when (nut.size  > bolt.size) then 1
        end
      end

      def match_all(nuts, bolts)
        if nuts.length == 0
          return []
        end

        nut = remove_random(nuts)

        bolt, smaller_bolts, bigger_bolts = split(nut, bolts)
        _, smaller_nuts, bigger_nuts = split(bolt, nuts)

        return [
          *match_all(smaller_nuts, smaller_bolts),
          [ nut, bolt ],
          *match_all(bigger_nuts, bigger_bolts)
        ]
      end


      def split(pivot, list)
        smaller = []
        bigger = []
        match = nil

        list.each do |item|
          # always put the nut on the left
          cmp = if (pivot.is_a?(Nut))
            compare_nut_and_bolt(pivot, item) * -1
          else
            compare_nut_and_bolt(item, pivot)
          end

          case cmp
          when 1  then bigger  << item
          when 0  then match    = item
          when -1 then smaller << item
          end
        end

        return [match, smaller, bigger]
      end

      def remove_random(list)
        list.delete_at(rand(list.length))
      end

    end
  end
end
