# Running

Have a relatively recent version of ruby on your machine.

1. `gem install bundler`
2. `bundle install --binstubs`

Run all the tests: `bin/rspec`

## Nuts & Bolts

Since we were so close to the final solution of this problem, I decided to code
it up and write a test for it.  It's nothing fancy but does finish off the
path we had been last discussing.  You can see a test for it in
`spec/traffic/traffic_spec.rb`.  It's just one non-fancy test but it seems
to work.

## Traffic

There was one bug preventing this from working fully when we left off.  I didn't
want to clean up the code any more than where we stopped, so I simply fixed
any bugs in the code and went from there.

If going through the rest of the design, the next avenue that I would explore is
allowing a light to be in multiple groups, where each group represents a set
of lights that are allowed to be green at the same time.  The tick logic would
change slightly, but would be similar.

