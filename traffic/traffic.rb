#!/usr/bin/env ruby

module Remind
  module Traffic
    class Light
      attr_accessor :state

      def initialize
        @state = :red
      end
    end

    class Group
      attr_accessor :lights, :green_time, :yellow_time

      def initialize(lights, green_time, yellow_time)
        @lights = lights
        @green_time = green_time
        @yellow_time = yellow_time
        @red_time = 1
        @activated_at = nil
      end

      def activate(tick)
        @activated_at = tick
      end

      def state=(state)
        @lights.each { |l| l.state = state}
      end

      def ticks_since_active(tick)
        tick - @activated_at
      end

      # All these should_be stat things could be combined into one method about
      # what state they should actually be in, making all these comparisons
      # far fewer times
      def should_be_green(tick)
        ticks_since_active(tick) < green_time
      end

      def should_be_yellow(tick)
        active_ticks = ticks_since_active(tick)
        @green_time <= active_ticks && active_ticks < (@green_time + @yellow_time)
      end

      def should_be_red(tick)
        active_ticks = ticks_since_active(tick)
        (@green_time + @yellow_time) <= active_ticks && active_ticks < (@green_time + @yellow_time + @red_time)
      end

      def should_yield(tick)
        active_ticks = ticks_since_active(tick)
        active_ticks >= (@green_time + @yellow_time + @red_time)
      end
    end

    class Intersection
      attr_reader :groups

      def initialize(groups)
        @groups = groups
        @active_group_i = 0
        @tick = 0

        active_group.activate(@tick)
        active_group.state = :green
      end

      def to_s
        return @groups.map.with_index do |g, gi|
          g.lights.map.with_index do |l, li|
            "G#{gi}-L#{li}:#{l.state}"
          end
        end.flatten.join(", ")
      end

      def tick()
        @tick += 1

        if (active_group.should_be_green(@tick))
          active_group.state = :green
        end

        if (active_group.should_be_yellow(@tick))
          active_group.state = :yellow
        end

        if (active_group.should_be_red(@tick))
          active_group.state = :red
        end

        if (active_group.should_yield(@tick))
          next_group
          active_group.activate(@tick)
          active_group.state = :green
        end
      end

      def active_group
        @groups[@active_group_i]
      end

      def next_group
        @active_group_i = (@active_group_i + 1) % @groups.size
      end
    end
  end
end
