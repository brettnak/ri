require 'rspec'
require './traffic/traffic'

RSpec.describe Remind::Traffic do
  describe 'intersection ticks' do
    subject(:light_1) { Remind::Traffic::Light.new }
    subject(:light_2) { Remind::Traffic::Light.new }
    subject(:light_3) { Remind::Traffic::Light.new }
    subject(:light_4) { Remind::Traffic::Light.new }

    subject(:intersection) do
      Remind::Traffic::Intersection.new([
        Remind::Traffic::Group.new([light_1, light_2], 3, 1),
        Remind::Traffic::Group.new([light_3, light_4], 2, 2),
      ])
    end

    before :each do
      # make sure this is initialized before each test
      intersection
    end

    # 2 light cycles worth of ticks
    [
      [:green, :green, :red, :red],
      [:green, :green, :red, :red],
      [:green, :green, :red, :red],
      [:yellow, :yellow, :red, :red],
      [:red , :red, :red, :red],
      [:red, :red, :green, :green],
      [:red, :red, :green, :green],
      [:red, :red, :yellow, :yellow],
      [:red, :red, :yellow, :yellow],
      [:red , :red, :red, :red],
      [:green, :green, :red, :red],
      [:green, :green, :red, :red],
      [:green, :green, :red, :red],
      [:yellow, :yellow, :red, :red],
      [:red , :red, :red, :red],
      [:red, :red, :green, :green],
      [:red, :red, :green, :green],
      [:red, :red, :yellow, :yellow],
      [:red, :red, :yellow, :yellow],
      [:red , :red, :red, :red],
    ].each.with_index do |states, ticks|
      describe "state after #{ticks} ticks" do
        it "is in the correct state" do
          ticks.times { intersection.tick() }
          expect(light_1.state).to eq(states[0])
          expect(light_2.state).to eq(states[1])
          expect(light_3.state).to eq(states[2])
          expect(light_4.state).to eq(states[3])
        end
      end
    end
  end
end
