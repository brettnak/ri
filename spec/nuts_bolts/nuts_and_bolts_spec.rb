require 'rspec'
require 'pry'
require './nuts_bolts/nuts_and_bolts'


RSpec.describe Remind::NutsAndBolts, '#match' do
  it "sorts nuts and bolts into matching pairs" do
    nuts, bolts = Remind::NutsAndBolts.random(1500)

    matches = Remind::NutsAndBolts.match_all(nuts, bolts)

    prev_nut = matches.first.first
    prev_bolt = matches.first.last

    expect(prev_nut.size).to eq(prev_bolt.size)

    matches.each.with_index do |(nut, bolt), i|
      next if i == 0

      expect(nut.size).to eq(bolt.size)
      expect(prev_nut.size).to be < nut.size
      expect(prev_bolt.size).to be < bolt.size

      prev_nut = nut
      prev_bolt = bolt
    end
  end
end
